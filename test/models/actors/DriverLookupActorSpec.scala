package models.actors

import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, ImplicitSender, TestKit}
import akka.util.Timeout
import models.DriverStatus
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import util.kdtree.{KDTree, Point, Entry}
import akka.pattern.ask
import scala.concurrent.duration._

import scala.concurrent.Await

class DriverLookupActorSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
with WordSpecLike with Matchers with BeforeAndAfterAll {
  def this() = this(ActorSystem("Gustavo"))

  implicit val timeout =  Timeout(20 seconds)

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  def prepareDriverLookupActor = {
    val driverStatuses = List(DriverStatus(123,20,20, true), DriverStatus(123,20,23, true))
    val entries =
      driverStatuses.map(e => Entry[DriverStatus](Point[Double](e.latitude, e.longitude), e))
    val spatialIndex = new KDTree[DriverStatus](entries)
    val driverLookupActor = TestActorRef(new DriverLookupActor)
    driverLookupActor ! UpdateSpatialIndex(spatialIndex)
    driverLookupActor
  }

  "DriverLookupActor" should {
    "request drivers in area" in {
      val actor = prepareDriverLookupActor
      val future = actor ? RequestDriversInArea(Point(19,19), Point(25,25))
      val result =  Await.result(future, timeout.duration).asInstanceOf[List[DriverStatus]]
      result should equal (List(DriverStatus(123,20,20, true), DriverStatus(123,20,23, true)))

    }
    "request nearest driver" in {
      val actor = prepareDriverLookupActor
      val future = actor ? RequestNearestDriver(Point(20.1,20.1))
      val result =  Await.result(future, timeout.duration).asInstanceOf[Option[DriverStatus]]
      result.get should equal (DriverStatus(123,20,20, true))
    }
  }

}
