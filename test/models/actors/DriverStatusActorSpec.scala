package models.actors

import akka.util.Timeout
import akka.testkit._
import models.DriverStatus
import akka.actor.ActorSystem
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.pattern.ask
import org.scalatest._

class DriverStatusActorSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
with WordSpecLike with Matchers with BeforeAndAfterAll {
  def this() = this(ActorSystem("DriverStatusActorSpec"))

  implicit val timeout =  Timeout(20 seconds)

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }
  "DriverStatusActor" should {
    "update driver status" in {
      val driverStatusActor = TestActorRef(new DriverStatusActor)
      driverStatusActor ! UpdateDriverStatus(123, DriverStatus(123,20,20, true))
    }

    "retrieve driver status" in {
      val driverStatusActor = TestActorRef(new DriverStatusActor)
      driverStatusActor ! UpdateDriverStatus(123, DriverStatus(123,20,20, true))
      val future = (driverStatusActor ? RequestDriverStatus(123))
      val result =  Await.result(future, timeout.duration).asInstanceOf[Option[DriverStatus]]
      result.get should equal (DriverStatus(123,20,20, true))
    }
  }

}
