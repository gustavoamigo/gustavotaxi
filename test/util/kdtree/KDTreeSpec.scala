package util.kdtree

import org.specs2.mutable._

import scala.util.Random

class KDTreeSpec extends Specification{

  "KDTree" should  {
    "find  nearest neighbor with 1000 entries" in {
      val random = new Random(123)
      def nextDouble = random.nextDouble
      val entries = (1 to 1000)
        .map(i => Entry[Double](Point(nextDouble*10.0, nextDouble*10.0), 0))
        .toList
      val kdtree = new KDTree[Double](entries)
      val naive = new NaiveSpatialIndex[Double](entries)
      (1 to 100).foreach { i =>
        val point = Point(nextDouble*10, nextDouble*10)
        val expected = naive.nearestNeighborSearch(point)
        val actual = kdtree.nearestNeighborSearch(point)
        actual must beEqualTo(expected)
      }
      ok
    }
    "search range with 1000 entries" in {
      val random = new Random(123)
      def nextDouble = random.nextDouble
      val p = (1 to 1000).map(i=>(nextDouble*10, nextDouble*10)).toList
      val entries = p.map(i => Entry[Double](Point(i._1, i._2), 0))
      val kdtree = new KDTree[Double](entries, 2)
      val naive = new NaiveSpatialIndex[Double](entries)
      (1 to 100).foreach { i =>
        val p1 = Point(nextDouble * 10 + nextDouble, nextDouble * 10 + nextDouble)
        val p2 = Point(p1(0)+1,p1(1)+1)
        val expected = naive.rangeSearch(p1,p2)
        val actual = kdtree.rangeSearch(p1,p2)
        val sumA = expected.map(e=> e.point(0) + e.point(1)).sum
        val sumB = actual.map(e=> e.point(0) + e.point(1)).sum
        actual.length must beEqualTo(expected.length)
        val diff = Math.abs(sumA - sumB)
        diff must beCloseTo(0, 0.001)
      }
      ok
    }

  }

}
