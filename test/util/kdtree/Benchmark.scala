package util.kdtree

object Benchmark extends App{

  def createSpatialIndex(impl:String, entries: List[Entry[Int]]):SpatialIndex[Int] = {
    impl match {
      case "baseline" => new NaiveSpatialIndex[Int](entries,2)
      case "kdtree" => new KDTree[Int](entries,2)
    }
  }

  def benchmark(treeSize:Int, searches:Int, repeats:Int, impl:String, shouldPrint: Boolean = true):Unit = {
    (1 to repeats).foreach { r =>
      val points = (1 to treeSize).map(i => (Math.random() * 10, Math.random() * 10))
      val entries = points.map(i => Entry(Point(i._1, i._2), 0)).toList

      var init = java.lang.System.currentTimeMillis()
      val spatialIndex = createSpatialIndex(impl, entries )
      var end = java.lang.System.currentTimeMillis()
      val treeCreation = end - init

      val pointsToSearch = (1 to searches).map(i => Point(Math.random() * 10, Math.random() * 10))

      init = java.lang.System.currentTimeMillis()
      pointsToSearch.foreach(p => spatialIndex.nearestNeighborSearch(p))
      end = java.lang.System.currentTimeMillis()
      val nns = end - init

      init = java.lang.System.currentTimeMillis()
      pointsToSearch.foreach(p => spatialIndex.rangeSearch(p, Point(p(0)+1,p(1)+1)))
      end = java.lang.System.currentTimeMillis()
      val search = end - init

      if(shouldPrint)
        print(impl +"\t" + treeSize.toString + "\t" + treeCreation + "\t" + nns +"\t" + search +"\n")
    }
  }

  def warmup = {
    println("Warming up JVM")
    benchmark(1000,100,100, "kdtree", shouldPrint = false)
    benchmark(1000,100,100, "baseline", shouldPrint = false)
    println("Done!")
  }

  def run = {
    println("impl\tsize\tcreate\tnns\trange")
    benchmark(100,100,10, "kdtree")
    benchmark(100,100,10, "baseline")
    benchmark(1000,100,10, "kdtree")
    benchmark(1000,100,10, "baseline")
    benchmark(10000,100,10, "kdtree")
    benchmark(10000,100,10, "baseline")
    benchmark(100000,100,10, "kdtree")
    benchmark(100000,100,10, "baseline")
  }

  warmup
  run

}
