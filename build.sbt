name := """GustavoTaxi"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.3.8" % "test",
  "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)


fork in run := true