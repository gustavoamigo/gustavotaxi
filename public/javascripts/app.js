angular.module('gustavotaxi', ['ngRoute', 'ngMessages','http-throttler'])
    .config(function ($routeProvider) {
        'use strict';
        $routeProvider.when('/', {
            templateUrl: 'principal.html',
        }).when('/benchmarkupdate', {
            templateUrl: 'terminal.html',
            controller: function($scope, $http) {
                $scope.lines = [];
                $scope.lines.push("Teste de carga do serviço Driver Update");
                $scope.lines.push("ATENÇÃO: Comece por este teste, depois espere cerca de um minuto para atualizar o índice.");
                $scope.lines.push("Do contrário os outros testes não retornarão valor algum.");
                $scope.sizes = [
                    {"code":10, "name":"10 - Só pra começar"},
                    {"code":100, "name":"100 - Puxa vida"},
                    {"code":1000, "name":"1000 - Ta fácil"},
                    {"code":10000, "name":"10000 - Ousado"}];

                $scope.size = 10;


                $scope.iniciar = function() {

                    var out = $scope.lines;
                    out.push("Iniciando teste de carga");
                    out.push("");
                    var j = 1;
                    var totalRequestTime = 0;
                    for(var i=1;i<=$scope.size;i++) {
                        var status = {"latitude":Math.random()*4-24.7318219-2,"longitude":Math.random()*4-45.9560509-2,"available":true};
                        $http.put('/drivers/' + i + '/status', status)
                            .success((function(data, status, headers) {
                                var requestTime = Number(headers("Request-Time"));
                                totalRequestTime = totalRequestTime + requestTime;
                                $scope.meanRequestTime = totalRequestTime / j;
                                out[out.length-1] = j + " Status de drivers atualizados com sucesso";
                                j++;
                            }).bind(this));

                    }
                };

            }
        }).when('/benchmarknns', {
            templateUrl: 'terminal.html',
            controller: function($scope, $http) {
                $scope.lines = [];
                $scope.lines.push("Teste de carga de buscas NNS");
                $scope.sizes = [
                    {"code":10, "name":"10 - Só pra começar"},
                    {"code":100, "name":"100 - Puxa vida"},
                    {"code":1000, "name":"1000 - Tá fácil"}];

                $scope.size = 10;


                $scope.iniciar = function() {
                    var out = $scope.lines;
                    out.push("Iniciando teste de carga de buscas NNS");
                    out.push("");
                    var j = 1;
                    var totalRequestTime = 0;
                    for(var i=1;i<=$scope.size;i++) {
                        var point = {"latitude":Math.random()*4-24.7318219-2,"longitude":Math.random()*4-45.9560509-2};
                        $http.get('/drivers/nearestDriver?point='+ point.latitude + ',' + point.longitude, status)
                            .success((function(data, status, headers) {
                                var requestTime = Number(headers("Request-Time"));
                                totalRequestTime = totalRequestTime + requestTime;
                                $scope.meanRequestTime = totalRequestTime / j;
                                out.push(j + " - Motorista encontrado no ponto ("+ data.longitude + ","+ data.latitude + ") com sucesso em " + requestTime + " ms");
                                j++;
                                while(out.length >= 37) out.shift();
                            }).bind(this))
                            .error((function(data, status, headers){
                                var requestTime = Number(headers("Request-Time"));
                                totalRequestTime = totalRequestTime + requestTime;
                                $scope.meanRequestTime = totalRequestTime / j;
                                switch(status) {
                                    case 404:
                                        out.push(j + " -  Nenhum motorista encontrado");
                                        break;
                                    default:
                                        out.push(j + " - Erro " + status);
                                }

                                j++;
                            }
                            ));

                    }
                };
            }
        }).when('/benchmarkrange', {
            templateUrl: 'terminal.html',
            controller: function($scope, $http) {
                $scope.lines = [];
                $scope.lines.push("Teste de carga de buscas Range Search");
                $scope.sizes = [
                    {"code":10, "name":"10 - Só pra começar"},
                    {"code":100, "name":"100 - Puxa vida"},
                    {"code":1000, "name":"1000 - Tá fácil"}];

                $scope.size = 10;


                $scope.iniciar = function() {
                    var out = $scope.lines;
                    out.push("Iniciando teste de carga de buscas Range");
                    out.push("");
                    var j = 1;
                    var totalRequestTime = 0;
                    for(var i=1;i<=$scope.size;i++) {
                        var sw = {"latitude":Math.random()*4-24.7318219-2,"longitude":Math.random()*4-45.9560509-2};
                        var ne = {"latitude":sw.latitude - 0.2, "longitude":sw.longitude - 0.2 };
                        $http.get('/drivers/inArea?sw='+ sw.latitude + ',' + sw.longitude + "&ne=" + ne.latitude + ',' + ne.longitude, status)
                            .success((function(data, status, headers) {
                                var requestTime = Number(headers("Request-Time"));
                                totalRequestTime = totalRequestTime + requestTime;
                                $scope.meanRequestTime = totalRequestTime / j;
                                out.push(j + " - " + data.length + " motoristas encontrados em " + requestTime + " ms");
                                j++;
                                while(out.length >= 37) out.shift();
                            }).bind(this));

                    }
                };
            }
        });
    }).config(function($httpProvider) {
      $httpProvider.interceptors.push('httpThrottler');
    });
