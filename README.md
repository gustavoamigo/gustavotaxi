** *Atenção*  **: Este readme só funciona na [página raiz do src](https://bitbucket.org/gustavoamigo/gustavotaxi/src) do projeto. Em outras 
partes links e imagens podem quebrar, coisa do Bitbucket.

Gustavo Taxis - Serviço de Localização de Taxista
=================================================

## Resumo

Projeto que demonstra o uso de [Scala](http://scala-lang.org/), do
 [Play Framework](http://playframework.com/) e do [Akka](http://akka.io/) na construção
 de um serviço de localização de taxis de alto desempenho.

Este projeto possui quatro serviços:

   * Atualiza status de taxista `PUT /drivers/:id/status`
   * Recupera status de taxista `GET /drivers/:id/status`
   * Recupera o taxista disponível mais próximo de um ponto `GET /drivers/nearestDriver?point=<lat>,<long>`
   * Recupera taxistas em uma determinada área `GET     /drivers/inArea?sw=<lat>,<long>&ne=<lat>,<long>`


## Solução Proposta

Como o status do motorista é algo completamente volátil e com pouco valor após um tempo, não é
necessário, nem mesmo recomendável, usar uma solução de banco de dados para manter o estado
da localização dos motoristas. Por isso, foi optado por uma solução em memória com a criação de um índice espacial para realizar a indexação geográfica de todos motoristas.

Além disso, foi optado pelo padrão [Actor](http://doc.akka.io/docs/akka/snapshot/scala/actors.html)
para manter o índice espacial e o índice por *driverId* em memória do status dos motoristas. Esta solução deve oferecer
um nível de desempenho satisfatório com pouco uso de recursos de infraestrutura. Além disso, 
a manutenção e a evolução da solução devem ser bastante simples.

## Índice Espacial

A implementação do índice espacial escolhida e implementada (e não copiada) para este projeto foi a [Árvore KD](http://www.inf.ufsc.br/~ine5384-hp/k-d/arvore-kd.html). Este tipo de estrutura
serve para armazenar em memória dados que possuem chave multidimensional. Além disso,
este tipo de estrutura facilita realizar duas operação - busca do vizinho mais perto
(_nearest neighbor search_) e busca em intervalo (_range search_). Há ampla referência
na internet sobre o funcionamento deste tipo de árvore, mas basicamente ele é uma
derivação da árvore binária de busca, também conhecida por BST. No caso da árvore KD,  a chave usada
como valor do nó alterna entre as dimensões de acordo com o nível. Por exemplo,
com um dado bidimensional com chaves _x_ e _y_, no nível zero da árvore o _x_ é usado
como valor do nó, no nível um da árvore o _y_ é usado como valor do nó e assim por diante. A implementação
feita neste projeto [KDTree.scala](src\master\app\util\kdtree\KDTree.scala) possui as seguintes características:

* É imutável, possui apenas o construtor e os métodos de busca
* Complexidade em tempo:
    * Contrução da árvore: O(n log n)
    * Nearest neighbor search: O(log n)
    * Range search: O(sqrt(n)+k) onde k é o número de pontos resultantes

### Benchmark da Árvore KD

Para garantir que o desempenho seja satisfatório para o nosso exercício realizei
um benchmark comparando o desempenho da árvore KD com uma implementação ingênua de busca espacial
([NaiveSpatialIndex.scala](src\master\app\util\kdtree\NaiveSpatialIndex.scala)). Apenas por curiosidade, 
o NaiveSpatialIndex é usada também para realizar testes unitários da KDTree. Esta implementação
possui complexidade algorítmica em tempo de O(1) para construção e O(N) para ambas as buscas.
No benchmark chamei esta implementação de baseline. O benchmark consiste em construir a estrutura
uma vez e realizar 100 operações de NNS (Nearest neighbor search) e 100 operações de
Range (Range search), variando a quantidade de itens armazenados na árvore. 
Por isso, os tempos expressos abaixo para as buscas são referentes
a 100 buscas e não apenas uma, para obter o tempo de uma busca é necessário dividir por 100.

![Benchmark baseline](https://bytebucket.org/gustavoamigo/gustavotaxi/raw/master/doc/bench_baseline.png)
![Benchmark kdtree](https://bytebucket.org/gustavoamigo/gustavotaxi/raw/master/doc/bench_kdtree.png)

*Observação*: Caso não consiga ver as imagens, elas estão disponíveis [aqui](https://bitbucket.org/gustavoamigo/gustavotaxi/src/master/doc/)

Teste realizado em um notebook com processador [Intel Core i5-3339Y @ 1.50GHz](http://www.cpubenchmark.net/cpu.php?cpu=Intel+Core+i5-3339Y+%40+1.50GHz)

Por esses teste podemos concluir que a implementação da árvore KD é muito melhor que a
implementação baseline. Mesmo com um número alto de valores indexados, como 100 mil,
é possível realizar as operações de busca em menos de 2ms por operação. Além disso,
a reconstrução da árvore não é algo custoso, ou seja, é completamente factível realizar sua completa
reconstrução em intervalos de 1 minuto.

## Actors

Foram criados três atores para este projeto:

 * [DriverLookupActor](src\master\app\models\actors\DriverLookupActor.scala) - Este
  ator tem a responsabilidade de realizar as buscas NNS e Range Search.
 * [DriverStatusActor](src\master\app\models\actors\DriverStatusActor.scala) - Este ator
  é responsável por manter o status dos taxistas.
 * [RefreshSpatialIndexActor](src\master\app\models\actors\RefreshSpatialIndexActor.scala) - Este
  ator tem como única responsabilidade realizar a recriação do índice espacial. Ele requisita
   as atualizações para o DriverStatusActor, recria a árvore KD e envia para o DriverLookupActor.
   Foi escolhido criar um ator específico para esta tarefa para que ela não interrompa
  as respostas dos atores acima. O disparo da recriação é feito pelo agendador do Akka e é
  feito a cada 1 minuto.

## Demo
A demonstração do projeto pode ser acessado [aqui](https://gustavotaxi.herokuapp.com/assets/demo.html#/benchmarkupdate).