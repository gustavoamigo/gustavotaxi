package util.kdtree
 
/**
 * Implementação criada apenas para comparar com o KDTree, implementa as buscas de forma
 * completamente ingênua.
 * @param entries
 * @param k
 * @tparam T
 */
class NaiveSpatialIndex[T](entries: List[Entry[T]],val k:Int = 2) extends SpatialIndex[T] {
   override def rangeSearch(p1: Point[Double], p2: Point[Double]): List[Entry[T]] = {
     def isContained(entry: Entry[T]) = {
       (0 to k - 1).forall(i => p1(i) <= entry.point(i) && entry.point(i) <= p2(i))
     }
     entries.filter(p => isContained(p))
   }

   override def nearestNeighborSearch(q: Point[Double],
                     distance: (Point[Double], Point[Double]) => Double = euclideanDistance): Option[Entry[T]] = {
     var minEntry = entries.head
     var minDistance = Double.PositiveInfinity
     entries.foreach(e=> {
       val dist = distance(e.point, q)
       if(dist<minDistance) {
         minEntry = e
         minDistance = dist
       }
     })
     Some(minEntry)
   }
 }
