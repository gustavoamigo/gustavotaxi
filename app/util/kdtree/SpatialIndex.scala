package util.kdtree

case class Point[T](x1: T = 0, x2: T = 0) {
  def apply(i: Int) = {
    i match {
      case 0 => x1
      case 1 => x2
    }
  }
}

case class Entry[T](point: Point[Double], value: T)

trait SpatialIndex[T] {
  def k:Int
  def rangeSearch(p1: Point[Double], p2: Point[Double]): List[Entry[T]]
  def nearestNeighborSearch(q: Point[Double],
                            distance: (Point[Double], Point[Double]) => Double = euclideanDistance): Option[Entry[T]]

  def euclideanDistance(p1:Point[Double], p2:Point[Double]) =
    Math.sqrt((0 to k-1).map {i => Math.pow(p1(i) - p2(i),2) }.sum)
}
