package util.kdtree

import scala.collection.immutable.Nil


/**
 * Implementação imutável de uma KDTree
 * @param entries Entradas para criação da KDTree
 * @param k Quantidade de dimensões, default 2.
 * @tparam T
 */
class KDTree[T](entries: List[Entry[T]],val k:Int = 2) extends SpatialIndex[T] {

  private val root = buildTree(entries.toArray, 0)

  private case class Node(splitDimension: Int,
                     splitValue: Double,
                     left: Option[Node],
                     right: Option[Node],
                     entry: Entry[T]) {
    def isLeaf = right.isEmpty && left.isEmpty
  }

  private def buildTree(entries: Array[Entry[T]],
                        depth: Int): Option[Node] = {
    val splitDimension = depth % k
    lazy val pivotElem = entries.head
    lazy val pivotValue = pivotElem.point(splitDimension)
    entries match {
      case elem if elem.isEmpty => None
      case elem if !elem.iterator.hasNext =>
        Some(Node(splitDimension, pivotValue, None, None, pivotElem))
      case _ =>
        val (leftEntries, rightEntries) = entries.drop(1).partition(e => e.point(splitDimension) <= pivotValue)
        val left = buildTree(leftEntries, depth + 1)
        val right = buildTree(rightEntries, depth + 1)
        Some(Node(splitDimension, pivotValue, left, right, pivotElem))
    }
  }


  override def rangeSearch(p1: Point[Double], p2: Point[Double]): List[Entry[T]] = {
    def isContained(n: Node) = {
      (0 to k - 1).forall(i => p1(i) <= n.entry.point(i) && n.entry.point(i) <= p2(i))
    }
    def searchRecursive(n: Node): List[Entry[T]] = {
      val nodeEntry: List[Entry[T]] = if (isContained(n)) List(n.entry) else Nil

      val left: List[Entry[T]] = if (p1(n.splitDimension) <= n.splitValue)
        n.left.map(searchRecursive).getOrElse(Nil)
      else Nil

      val right: List[Entry[T]] = if (n.splitValue <= p2(n.splitDimension))
        n.right.map(searchRecursive).getOrElse(Nil)
      else Nil

      nodeEntry ::: left ::: right
    }
    root.map(searchRecursive).getOrElse(Nil)
  }

  override def nearestNeighborSearch(q: Point[Double],
                            distance: (Point[Double], Point[Double])
                              => Double = euclideanDistance): Option[Entry[T]] = {

    var p: Option[Node] = None
    var w = Double.PositiveInfinity

    def nnsRecursive(n: Node): Unit = {
      val w1 = distance(q, n.entry.point)
      if (w1 < w) {
        w = w1
        p = Some(n)
      }
      if (!n.isLeaf) {
        q(n.splitDimension) <= n.splitValue match {
          case true =>
            if (q(n.splitDimension) - w <= n.splitValue) n.left.map(nnsRecursive)
            if (q(n.splitDimension) + w > n.splitValue) n.right.map(nnsRecursive)
          case false =>
            if (q(n.splitDimension) + w > n.splitValue) n.right.map(nnsRecursive)
            if (q(n.splitDimension) - w <= n.splitValue) n.left.map(nnsRecursive)
        }
      }
    }

    root.map(nnsRecursive)

    p match {
      case None => None
      case _ => Some(p.get.entry)
    }
  }

  override def toString = {
    root.toString
  }
}
