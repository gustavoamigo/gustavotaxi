package controllers

import _root_.util.kdtree.Point
import models._
import models.actors._
import org.joda.time.DateTime
import play.api.libs.concurrent.Akka
import play.api.libs.json._
import play.api.mvc._
import play.api.Play.current
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import play.api.libs.concurrent.Execution.Implicits.defaultContext

case class DriverStatusViewModel(latitude:Double, longitude:Double, available:Boolean)

object DriversController extends Controller {

  def driverStatusesActor = Akka.system.actorSelection("/user/"+DriverStatusActor.NAME)
  def driversLocationLookupActor = Akka.system.actorSelection("/user/"+DriverLookupActor.NAME)

  implicit val driverStatusViewModelWrites = Json.writes[DriverStatusViewModel]
  implicit val driverStatusViewModelReads = Json.reads[DriverStatusViewModel]
  implicit val driverStatusWrites = Json.writes[DriverStatus]
  implicit val driverStatusReads = Json.reads[DriverStatus]

  implicit val timeout =  Timeout(1 minute)

  /**
   * PUT  /drivers/:id/status
   */
  def updateStatus(id:Long) = Action(parse.json[DriverStatusViewModel]) {
    request =>
      val driverStatus: DriverStatusViewModel = request.body
      driverStatusesActor ! UpdateDriverStatus(
                              id,
                              DriverStatus(id,
                                driverStatus.latitude,
                                driverStatus.longitude,
                                driverStatus.available))
      NoContent
  }

  /**
   * GET  /drivers/:id/status
   */
  def getStatus(id:Long): Action[AnyContent] = Action.async {
    (driverStatusesActor ? RequestDriverStatus(id ))
      .mapTo[Option[DriverStatus]]
      .map {
        case None => NotFound
        case status =>
          val driverStatusVM =
            DriverStatusViewModel(status.get.latitude,status.get.longitude, status.get.available)
          Ok(Json.toJson(driverStatusVM))
    }
  }

  /**
   * GET  /drivers/nearestDriver?point=<lat>,<long>
   */
  def nearestDriver(point:String): Action[AnyContent] = Action.async {
    val splitPoint = point.split(",")
    val latitude = splitPoint(0).toDouble
    val longitude = splitPoint(1).toDouble
    (driversLocationLookupActor ? RequestNearestDriver(Point(latitude, longitude)))
      .mapTo[Option[DriverStatus]]
      .map {
        case None => NotFound
        case status => Ok(Json.toJson(status))
    }
  }

  /**
   * GET  /drivers/inArea?sw=<lat>,<long>&ne=<lat>,<long>
   */
  def inArea(sw:String, ne:String): Action[AnyContent] = Action.async {
    val splitSw = sw.split(",")
    val splitNe = ne.split(",")
    val swPoint = Point[Double](splitSw(0).toDouble, splitSw(1).toDouble)
    val nePoint = Point[Double](splitNe(0).toDouble, splitNe(1).toDouble)
    (driversLocationLookupActor ? RequestDriversInArea(nePoint, swPoint))
      .mapTo[List[DriverStatus]]
      .map {
        statuses =>
          Ok(Json.toJson(statuses.take(100)))
    }
  }

}
