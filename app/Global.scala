import controllers.Default
import models.actors._
import play.api.GlobalSettings
import play.api.libs.concurrent.Akka
import play.api.Play.current
import util.filters.LoggingFilter
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.Logger
import play.api.mvc.{WithFilters, SimpleResult, RequestHeader, Filter}

object Global extends WithFilters(LoggingFilter) with GlobalSettings {
  override def onStart(app: play.api.Application): scala.Unit =
  {
    val driversStatusesActor = Akka.system.actorOf(DriverStatusActor.props, DriverStatusActor.NAME )
    val driversLocationLookupActor = Akka.system.actorOf(DriverLookupActor.props, DriverLookupActor.NAME )
    val refreshSpatialIndexActor = Akka.system.actorOf(RefreshSpatialIndexActor.props, RefreshSpatialIndexActor.NAME )

    Akka.system.scheduler.schedule(20 seconds, 1 minutes, refreshSpatialIndexActor, TriggerRefreshIndex)
  }
}



