package models.actors

import akka.actor.{Props, Actor}
import util.kdtree._
import models._

case class RequestNearestDriver(latlong: Point[Double])
case class RequestDriversInArea(ne:Point[Double], sw:Point[Double])
case class UpdateSpatialIndex(spatialIndex:KDTree[DriverStatus])

object DriverLookupActor {
  def props:Props = Props(new DriverLookupActor())
  val NAME = "driverLookupActor"
}

/**
 * Ator responsável por realizar a busca de taxista
 */
class DriverLookupActor extends Actor {

  private var spatialIndex = new KDTree[DriverStatus](Nil)

  def receive = {

    case RequestNearestDriver(latlong) =>
      val result = spatialIndex.nearestNeighborSearch(latlong)
      sender() ! result.map(i => i.value)

    case RequestDriversInArea(ne, sw) =>
      val result = spatialIndex.rangeSearch(ne, sw)
      sender() ! result.map(i => i.value)

    case UpdateSpatialIndex(newSpatialIndex) =>
      spatialIndex = newSpatialIndex
  }
}
