package models.actors

import akka.actor.{Props, Actor}
import akka.pattern.ask
import akka.util.Timeout
import models.DriverStatus
import util.kdtree.{KDTree, Point, Entry}
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

case class TriggerRefreshIndex()

object RefreshSpatialIndexActor {
  def props:Props = Props(new RefreshSpatialIndexActor())
  val NAME = "refreshSpatialIndexActor"
}

/**
 * Ator responsável por realizar a reindexação do índice espacial dos motoristas
 */
class RefreshSpatialIndexActor extends Actor {

  private case class DoRefreshIndex(driverStatuses:Map[Long, DriverStatus])

  implicit val timeout =  Timeout(5 minutes)

  def driverLookupActor = context.actorSelection("/user/"+DriverLookupActor.NAME)
  def driverStatusActor = context.actorSelection("/user/"+DriverStatusActor.NAME)

  def receive = {
    case TriggerRefreshIndex =>
      (driverStatusActor ? RequestAllDriversStatuses)
        .mapTo[Map[Long, DriverStatus]]
        .map {
          driversUpdate=>
            self ! DoRefreshIndex(driversUpdate)
        }


    case DoRefreshIndex(driverStatuses) =>
      val entries = driverStatuses
        .filter(f => f._2.available)
        .map {
          e => Entry[DriverStatus](Point[Double](e._2.latitude, e._2.longitude),e._2)
      }.toList
      val spatialIndex = new KDTree[DriverStatus](entries)
      driverLookupActor ! UpdateSpatialIndex(spatialIndex)
  }

}
