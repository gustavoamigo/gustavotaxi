package models.actors

import akka.actor._
import models.DriverStatus
import org.joda.time.DateTime

case class UpdateDriverStatus(id:Long, newStatus:DriverStatus)
case class RequestDriverStatus(id:Long)
case class RequestAllDriversStatuses()

object DriverStatusActor {
  def props:Props = Props(new DriverStatusActor())
  val NAME = "driverStatusActor"
}

/**
 * Ator responsável por manter e atualizar o status de cada taxista
 */
class DriverStatusActor extends Actor {

  private var driverStatusesMap = Map[Long, DriverStatus]()

  def receive = {
    case UpdateDriverStatus(id,newStatus) =>
      driverStatusesMap = driverStatusesMap.+ (id -> newStatus)

    case RequestDriverStatus(id) =>
      sender() ! driverStatusesMap.get(id)

    case RequestAllDriversStatuses =>
      sender() ! driverStatusesMap
  }
}
