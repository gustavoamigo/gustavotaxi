package models

case class DriverStatus(driverId:Long, latitude:Double, longitude:Double, available:Boolean)
